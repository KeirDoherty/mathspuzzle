package com.kd.mathspuzzle;

import lombok.Value;

@Value
public class MathsPuzzleGuessDto {

    Long puzzleId;
    double guess;
}
