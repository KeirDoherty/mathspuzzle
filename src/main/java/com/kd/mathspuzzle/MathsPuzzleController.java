package com.kd.mathspuzzle;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/mathspuzzle")
public class MathsPuzzleController {

    @Autowired
    private MathsPuzzleGeneratorService mathsPuzzleGeneratorService;

    @Autowired
    private MathsPuzzleService mathsPuzzleService;

    @Autowired
    private MathsPuzzleStateService mathsPuzzleStateService;

    @GetMapping()
    MathsPuzzle generateRandomMathsPuzzle() {
        MathsPuzzle mathsPuzzle = mathsPuzzleGeneratorService.randomMathsPuzzle();
        mathsPuzzleStateService.addMathsPuzzle(mathsPuzzle);
        log.info("Generated and stored maths puzzle {}", mathsPuzzle);

        return mathsPuzzle;
    }

    @PostMapping()
    MathsPuzzleResult verifyClientGuess(MathsPuzzleGuessDto guessDto) {
        MathsPuzzle puzzle = mathsPuzzleStateService.getMathsPuzzleById(guessDto.getPuzzleId());
        return mathsPuzzleService.verifyGuess(puzzle, guessDto);
    }
}