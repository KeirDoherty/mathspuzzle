package com.kd.mathspuzzle;

public interface MathsPuzzleService {

    MathsPuzzleResult verifyGuess(MathsPuzzle puzzle, MathsPuzzleGuessDto mathsPuzzleGuessDto);

}
