package com.kd.mathspuzzle;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Arithmetic {

    MULTIPLICATION("*"),
    DIVISION("÷");

    private final String name;

    Arithmetic(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
