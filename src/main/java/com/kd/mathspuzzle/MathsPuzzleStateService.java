package com.kd.mathspuzzle;

public interface MathsPuzzleStateService {

    void addMathsPuzzle(MathsPuzzle mathsPuzzle);

    MathsPuzzle getMathsPuzzleById(Long puzzleId);
}