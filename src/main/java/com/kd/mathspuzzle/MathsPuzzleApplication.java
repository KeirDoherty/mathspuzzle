package com.kd.mathspuzzle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MathsPuzzleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MathsPuzzleApplication.class, args);
    }

}
