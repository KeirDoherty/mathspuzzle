package com.kd.mathspuzzle;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MathsPuzzleRepository extends JpaRepository<MathsPuzzle, Long> {

    @Override
    <S extends MathsPuzzle> S saveAndFlush(S s);

    MathsPuzzle findMathsPuzzleByPuzzleId(Long puzzleId);
}
