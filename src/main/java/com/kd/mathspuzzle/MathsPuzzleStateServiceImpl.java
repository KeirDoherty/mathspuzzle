package com.kd.mathspuzzle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MathsPuzzleStateServiceImpl implements MathsPuzzleStateService {

    @Autowired
    private MathsPuzzleRepository mathsPuzzleRepository;

    @Override
    public void addMathsPuzzle(MathsPuzzle mathsPuzzle) {
        mathsPuzzleRepository.saveAndFlush(mathsPuzzle);
    }

    @Override
    public MathsPuzzle getMathsPuzzleById(Long puzzleId) {
     return mathsPuzzleRepository.findMathsPuzzleByPuzzleId(puzzleId);
    }
}
