package com.kd.mathspuzzle;


import lombok.*;

@AllArgsConstructor
@Data
public class MathsPuzzleResult {

    private double answer;
    private boolean correct;
    private MathsPuzzleGuessDto mathsPuzzleGuessDto;

}
