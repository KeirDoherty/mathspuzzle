package com.kd.mathspuzzle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Slf4j
@Service
public class MathsPuzzleServiceImpl implements MathsPuzzleService {

    @Override
    public MathsPuzzleResult verifyGuess(MathsPuzzle puzzle, MathsPuzzleGuessDto guessDto) {

        double answer;

        switch (puzzle.getArithmetic()) {
            case MULTIPLICATION -> answer = solveMultiplication(puzzle);
            case DIVISION -> answer = solveDivisionRoundUp2dp(puzzle);
            default -> throw new IllegalStateException("Unexpected value: " + puzzle.getArithmetic());
        }

        log.info("Verifying guess {} for puzzle {}", guessDto, puzzle);
        return new MathsPuzzleResult(answer, isCorrect(answer, guessDto), guessDto);
    }

    private double solveMultiplication(MathsPuzzle puzzle) {
        return puzzle.getNumberA() * puzzle.getNumberB();
    }

    private double solveDivisionRoundUp2dp(MathsPuzzle puzzle) {
        double answer = (double) puzzle.getNumberA() / (double) puzzle.getNumberB();
        BigDecimal bd = new BigDecimal(answer).setScale(2, RoundingMode.UP);
        return bd.doubleValue();
    }

    private boolean isCorrect(double answer, MathsPuzzleGuessDto dto) {
        return answer == dto.getGuess();
    }
}
