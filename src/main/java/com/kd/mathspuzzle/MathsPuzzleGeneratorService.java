package com.kd.mathspuzzle;

public interface MathsPuzzleGeneratorService {
     MathsPuzzle randomMathsPuzzle();
}
