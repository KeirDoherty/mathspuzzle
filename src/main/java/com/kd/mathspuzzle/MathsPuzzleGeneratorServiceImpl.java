package com.kd.mathspuzzle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Random;

@Slf4j
@Service
public class MathsPuzzleGeneratorServiceImpl implements MathsPuzzleGeneratorService {


    private static final int MINIMUM_NUMBER = 2;
    private static final int MAXIMUM_NUMBER = 15;

    private final Random random;

    public MathsPuzzleGeneratorServiceImpl() {
        this.random = new Random();
    }

    public MathsPuzzleGeneratorServiceImpl(Random random) {
        this.random = random;
    }

    @Override
    public MathsPuzzle randomMathsPuzzle() {
        return new MathsPuzzle(null, getRandomArithmetic(), getRandomInt(), getRandomInt());
    }

    private Arithmetic getRandomArithmetic() {
        return Arrays.stream(Arithmetic.values())
                .skip(random.nextInt(Arithmetic.values().length))
                .findFirst()
                .orElseThrow();
    }

    private int getRandomInt() {
        return random.nextInt(MAXIMUM_NUMBER - MINIMUM_NUMBER) + MINIMUM_NUMBER;
    }
}