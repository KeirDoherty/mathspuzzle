package com.kd.mathspuzzle;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class MathsPuzzle {

    @Id
    @GeneratedValue
    private Long puzzleId;
    private Arithmetic arithmetic;
    private int numberA;
    private int numberB;

}