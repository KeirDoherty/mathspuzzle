package com.kd.mathspuzzle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.kd.mathspuzzle.Arithmetic.*;
import static org.assertj.core.api.BDDAssertions.then;

public class MathsPuzzleServiceTest {

    MathsPuzzleService mathsPuzzleService;

    @BeforeEach
    public void setUp() {
        mathsPuzzleService = new MathsPuzzleServiceImpl();
    }

    @Test
    public void testCorrectMultiplication() {

        //given
        MathsPuzzle puzzle = new MathsPuzzle(1L, MULTIPLICATION, 2, 2);
        MathsPuzzleGuessDto mathsPuzzleGuessDto = new MathsPuzzleGuessDto(1L, 4);

        //when
        MathsPuzzleResult resultMathsPuzzleResult = mathsPuzzleService.verifyGuess(puzzle, mathsPuzzleGuessDto);

        //then
        then(resultMathsPuzzleResult.isCorrect()).isTrue();
    }

    @Test
    public void testIncorrectMultiplication() {
        //given
        MathsPuzzle puzzle = new MathsPuzzle(1L, MULTIPLICATION, 2, 2);
        MathsPuzzleGuessDto mathsPuzzleGuessDto = new MathsPuzzleGuessDto(1L, 2);

        //when
        MathsPuzzleResult resultMathsPuzzleResult = mathsPuzzleService.verifyGuess(puzzle, mathsPuzzleGuessDto);

        //then
        then(resultMathsPuzzleResult.isCorrect()).isFalse();
    }

    @Test
    public void testCorrectDivision() {

        //given
        MathsPuzzle puzzle = new MathsPuzzle(1L, DIVISION, 2, 3);
        MathsPuzzleGuessDto mathsPuzzleGuessDto = new MathsPuzzleGuessDto(1L, 0.67);

        //when
        MathsPuzzleResult resultMathsPuzzleResult = mathsPuzzleService.verifyGuess(puzzle, mathsPuzzleGuessDto);

        //then
        then(resultMathsPuzzleResult.isCorrect()).isTrue();
    }

    @Test
    public void testIncorrectDivision() {
        //given
        MathsPuzzle puzzle = new MathsPuzzle(1L, DIVISION, 3, 2);
        MathsPuzzleGuessDto mathsPuzzleGuessDto = new MathsPuzzleGuessDto(1L, 1.25);

        //when
        MathsPuzzleResult resultMathsPuzzleResult = mathsPuzzleService.verifyGuess(puzzle, mathsPuzzleGuessDto);

        //then
        then(resultMathsPuzzleResult.isCorrect()).isFalse();
    }
}