package com.kd.mathspuzzle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class MathsPuzzleGeneratorServiceTest {

    private MathsPuzzleGeneratorService mathsPuzzleGeneratorService;

    @BeforeEach
    public void setUp() {
        mathsPuzzleGeneratorService = new MathsPuzzleGeneratorServiceImpl();
    }

    @Test
    public void generateRandomFactorIsBetweenExpectedLimits() {
        MathsPuzzle puzzle = mathsPuzzleGeneratorService.randomMathsPuzzle();
        assertThat(puzzle).isNotNull(); // TODO
    }
}
